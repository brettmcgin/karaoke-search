import React, { Component } from 'react';
import './Karaoke.css';
import songs from './karaoke.json';

export default class Karaoke extends Component {
  constructor(props) {
    super(props);

    this.state = { search: '', songs: songs };
  }

  render() {
    return (
      <div>
        <input
          type="search"
          placeholder="Song Search"
          onChange={this.handleChange.bind(this)}
        />
        {this.songList()}
      </div>
    );
  }

  songList() {
    if (this.state.search) {
      const songsThatMatch = this.state.songs.filter(song => {
        return (
          song.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
          song.artist.toLowerCase().includes(this.state.search.toLowerCase())
        );
      });

      const listItems = songsThatMatch.map((song, index) => {
        return (
          <li className="song" key={`song-${index}`}>
            <div className="song_info">
              <div className="song__title">{song.title}</div>
              <div className="song__artist">{song.artist}</div>
            </div>
            <div className="song__number">{song.songNumber}</div>
          </li>
        );
      });

      return <ul>{listItems}</ul>;
    } else {
      return <div>Search for a Song or Artist</div>;
    }
  }

  handleChange(e) {
    this.setState({ search: e.target.value });
  }
}
